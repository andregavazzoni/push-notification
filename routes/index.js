let express = require('express');
let router = express.Router();
let fs = require('fs');

/* GET home page. */
router.get('/', function (req, res, next) {
    let publicKey = '';

    if (fs.existsSync('./vapid.json')) {
        publicKey = JSON.parse(fs.readFileSync('./vapid.json').toString()).publicKey;
    }

    res.render('index', {
        title: 'Push Notification Sandbox',
        publicKey: publicKey
    });
});

/* POST Save user endpoint */
router.post('/save-endpoint', function (req, res) {
    fs.writeFileSync('./subscription.json', JSON.stringify(req.body));
    res.setHeader('Content-type', 'application/json');
    res.json('{"success": "ok}');
});

module.exports = router;